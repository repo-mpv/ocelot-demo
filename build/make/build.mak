build-service:
	@echo service name: $(var_service_name)
	@docker-compose build $(BUILD_FLAGS) $(var_service_name)

build-gateway: var_service_name=gateway
build-gateway: build-service

build-identity: var_service_name=identity
build-identity: build-service

build-location: var_service_name=location
build-location: build-service