﻿using System;
using Microsoft.OpenApi.Models;

namespace common
{
    public class SwaggerEndpoint
    {
        public string Url { get; set; } = "/swagger/v1/swagger.json";
        public string Name { get; set; }
    }
    public class DocsConfig
    {
        public string[] ServerUrls { get; set; } = new string[] { };
        public SwaggerEndpoint SwaggerEndpoint { get; set; }
        public OpenApiInfo OpenApiInfo { get; set; }
    }
}
