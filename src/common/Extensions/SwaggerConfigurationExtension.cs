﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace common
{
    public static class SwaggerConfigurationExtenstion
    {
        public static void UseSwaggerExt(this IApplicationBuilder app, IConfiguration Configuration)
        {
            var docsConfig = Configuration.GetSection("Config:Docs").Get<DocsConfig>();
            app.UseSwagger(c =>
                {
                    c.PreSerializeFilters.Add((swagger, httpReq) =>
                        {
                            swagger.Servers = docsConfig.ServerUrls.Select(url => new OpenApiServer { Url = url }).ToList(); }
                        );
                }).UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint(docsConfig.SwaggerEndpoint.Url, docsConfig.SwaggerEndpoint.Name);
                });

        }
        public static IServiceCollection AddSwaggerGenExt(this IServiceCollection services, IConfiguration Configuration)
        {
            var docsConfig = Configuration.GetSection("Config:Docs").Get<DocsConfig>();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(docsConfig.OpenApiInfo.Version, docsConfig.OpenApiInfo);
            });
            return services;
        }
    }
}
