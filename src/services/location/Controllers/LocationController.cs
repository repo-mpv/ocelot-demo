﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace location.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LocationController : ControllerBase
    {
        private static readonly UserLocation[] UserLocations = new UserLocation[]
        {
            new UserLocation { Id = 1, UserId = 1, LocationId = 1, UpdateDate = DateTime.UtcNow }
        };

        private readonly ILogger<LocationController> _logger;

        public LocationController(ILogger<LocationController> logger)
        {
            _logger = logger;
        }

        [HttpGet("user/{id}")]
        public IActionResult GetUserLocation(long id)
        {
            var userLocation = UserLocations.FirstOrDefault(l => l.UserId == id);
            if (userLocation == null)
                return NotFound();
            return Ok(userLocation);
        }
    }
}
