using System;

namespace location
{
    public class UserLocation
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public long LocationId { get; set; }

        public DateTime UpdateDate { get; set; }
    }
}
