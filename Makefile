include ./build/make/build.mak

NO_CACHE?=0

BUILD_FLAGS :=
ifeq ($(NO_CACHE), 1)
	BUILD_FLAGS+= --no-cache
endif

all:
	docker-compose build $(BUILD_FLAGS)

up:
	@docker-compose up

gate:
	$(MAKE) build-gateway

services: identity location

identity:
	$(MAKE) build-identity

location:
	$(MAKE) build-location