**ocelot-demo**

ocelot-demo is an app for demonstrating response aggregation and using MMLib.SwaggerFor Ocelot

.Net Core 3.1/Ocelot/MMLib.SwaggerForOcelot

**BUILD**

```
docker-compose build
```

**Start**

```
docker-compose up
```

Request aggregation:

```
curl http://localhost/api/v1/user-info/1
```

```
{
   "user": {
      "id": 1,
      "firstName": "Ivan",
      "lastName": "Ivanov"
   },
   "userLocation": {
      "id": 1,
      "userId": 1,
      "locationId": 1,
      "updateDate": "2020-10-01T13:08:04.5290909Z"
   }
}
```

**Swagger:**

```
http://localhost/swagger/index.html
```

![ocelot-demo](https://bitbucket.org/repo-mpv/ocelot-demo/raw/1664563891bc38cfcd9883ead6355ef7014a2582/img/ocelot-demo.swagger.png)